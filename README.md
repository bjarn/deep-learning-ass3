Assignment in Deep Learning for Data science.

The directory structure and functions for numerical calculation of gradients was given. The rest was coded by me.

The task at hand is to build, train and test k-layer networks, using mini-batch gradient descent to
a cost function using cross-entropy loss, with an L2 regularization term on the weight matrices. It
will use the ReLu-activation. To make training possible on >2 layers batch normalization will be
used. The data to be trained on is the CIFAR-10 dataset. Matlab was used.
