function acc = ComputeAccuracyBN(X, y, W, b, varargin)
%Computes the accuracy of a network using eq. 4 in the assignment
    if isempty(varargin)
        [~,~,~,~,~, K_star] = ForwardPassBN(X, W, b, 1);
    else
        [~,~,~,~,~, K_star] = ForwardPassBN(X, W, b, 1, varargin{1}, varargin{2});
    end
    [~, K_star] = max(K_star, [], 1);
    acc = sum(y == K_star)/size(X,2);
end

