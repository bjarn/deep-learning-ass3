function [s, P] = EvaluateClassifier1Layer2(X, W, b)
%Calculates the percentages for each label for each sample
%Might require R2016b to work correctly
    h = X;
    s = W* h + b;
    P = softmax(s);
end

