function J = ComputeCostBN(X, Y, W, b, lambda, BN, varargin)
%Computes cost using eq. 5 in assignment
    if isempty(varargin)
        [~,~,~,~,~, P] = ForwardPassBN(X, W, b, BN);
    else
        [~,~,~,~,~, P] = ForwardPassBN(X, W, b, BN, varargin{1}, varargin{2});
    end
    l_cross = sum(-log(diag(Y'*P)));
    J = sum(l_cross)/size(X,2);
    for i = size(W,2)
        J = J + lambda*(sum(sum(W{i}.*W{i})));
    end

end

