function acc = ComputeAccuracy(X, y, W, b)
%Computes the accuracy of a network using eq. 4 in the assignment
    [~,~,K_star] = EvaluateClassifier(X, W, b);
    [~, K_star] = max(K_star, [], 1);
    acc = sum(y == K_star)/size(X,2);
end

