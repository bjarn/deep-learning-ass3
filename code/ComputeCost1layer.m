function J = ComputeCost1layer(X, Y, W, b, lambda)
%Computes cost using eq. 5 in assignment
    l_cross = sum(-log(diag(Y'*EvaluateClassifier1Layer2(X, W, b)))');
    J = sum(l_cross)/size(X,2)+(lambda*sum(sum(W.*W)));
end

