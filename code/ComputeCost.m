function J = ComputeCost(X, Y, W, b, lambda)
%Computes cost using eq. 5 in assignment
    X;
    [~,~, P] = EvaluateClassifier(X, W, b);
    %[~,~, P] = onelayer(X, W, b)
    l_cross = sum(-log(diag(Y'*P)));
    J = sum(l_cross)/size(X,2);
    for i = size(W,2)
        J = J + lambda*(sum(sum(W{i}.*W{i})));
    end
end

