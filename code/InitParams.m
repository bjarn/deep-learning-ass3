function [W, W_moment, b, b_moment, rho, decay] = InitParams(X, Y)
    %Initializes most parameters
    rho =0.9; decay = 0.95;
    m=50; d=size(X,1); n=30; K = 10; 
    %3 layer network
    weight_params = [m,d; n,m; K,n];
    %2 layer network
    %weight_params = [n,d; K,n];
    for i = 1:size(weight_params,1)
        dims = weight_params(i, :);
        W{i} = .001*randn(dims);
        W_moment{i} = zeros(size(W{i}));
        b{i} = zeros(dims(1),1);
        b_moment{i} = zeros(size(b{i}));
    end
end

