function Main()
    %THE MAIN SCRIPT
    %LOADING DATA
    [X, Y, y] = LoadBatch('data_batch_1.mat');
    [vX, vY, vy] = LoadBatch('data_batch_2.mat');
    [tX, tY, ty] = LoadBatch('test_batch.mat');
    %Normalize data
    X_mean = mean(X,2);
    X = double(X) - repmat(X_mean, [1, size(X, 2)]);
    vX = vX - repmat(X_mean, [1, size(vX, 2)]);
    tX = tX - repmat(X_mean, [1, size(tX, 2)]);
    format long
    %%
    %Test the finer hyperparam values
    n_epochs = 5;
    %eta = 0.036826249945; lambda = 0.001883553952;
    eta = 0.065916629041086; lambda =  0.001369941385255;
    [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta, lambda, n_epochs, 0);
    [Wstarbn, bstarbn, cost_trainbn, cost_valbn, acc_valbn] = TestNet( X, Y, y, vX, vY, vy, eta, lambda, n_epochs, 1);
    
    %%
    %Test the 2 layer with and without BN and differing eta(low, med, high)
    n_epochs = 10;
    eta = [0.01, 0.1, 0.3];
    lambda =  0.001;
    parfor i = 1:size(eta,2)
        [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta(i), lambda, n_epochs, 1);
        resultstrain{i} = [eta(i), 0, cost_train]
        resultsval{i} = [eta(i), 0, cost_val]
    end
    %%
    %Plot results of 2 layer
    figure;
    etan = 3;
    plot(0:n_epochs,resultsval{etan}(3:end), 'g',0:n_epochs,resultstrain{etan}(3:end),'b')
    str=sprintf('Cost on val-set(g) and train-set(b) with BN and eta = %f', resultsval{etan}(1));
    title(str);
    xlabel('Epochs');
    ylabel('Cost');
    
    %%
    %All training available train network fully
    n_epochs = 20;
    trainX = [X, vX, tX(:,1:9000)];
    trainY = [Y, vY, tY(:,1:9000)];
    trainy = [y, vy, ty(:,1:9000)];
    testX = tX(:,9001:end);
    testY = tY(:,9001:end);
    testy = ty(:,9001:end);
    %eta = 0.036826249945; lambda = 0.001883553952;
    eta = 0.065916629041086; lambda = 0.001369941385255;
    [Wstarbn, bstarbn, cost_trainbn, cost_valbn, acc_valbn] = TestNet( trainX, trainY, trainy, testX, testY, testy, eta, lambda, n_epochs, 1);
    %[Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( trainX, trainY, trainy, testX, testY, testy, eta, lambda, n_epochs, 0);
        %%
    % Plot cost on train and validation set, and accuracy
    figure;
    plot(0:n_epochs,cost_valbn, 'g',0:n_epochs,cost_val,'b')
    %axis([1 n_epochs 1.6 2.3])
    str=sprintf('Comparing cost with/without BN: Lambda = %f and eta = %f', lambda, eta);
    title(str);
    xlabel('Epochs');
    ylabel('Cost');
    
    figure;
    plot(0:n_epochs,acc_valbn, 'g',0:n_epochs,acc_val,'b')
    str=sprintf('Comparing accuracy with/without BN: Lambda = %f and eta = %f', lambda, eta);
    title(str);
    xlabel('Epochs');
    ylabel('Accuracy');
      %%
    % Plot cost on train and validation set, and accuracy
    figure;
    plot(0:n_epochs,cost_valbn, 'g')
    str=sprintf('Cost with BN: Lambda = %f and eta = %f', lambda, eta);
    title(str);
    xlabel('Epochs');
    ylabel('Cost');
    figure;
    plot(0:n_epochs,acc_valbn, 'g')
    str=sprintf('Accuracy with BN: Lambda = %f and eta = %f', lambda, eta);
    title(str);
    xlabel('Epochs');
    ylabel('Accuracy');
    %%
    %Find reasonable learning rates
    n_epochs = 4;
    
    eta = 0.001:.1:1;
    results = cell(size(eta,2), 1);
    parfor i = 1:size(eta,2)
        [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta(i), 0.000001, n_epochs, 1);
        results{i} = [eta(i), cost_val]
    end
    results = cell2mat(results)
    fileID = fopen('reasonable_learning.txt','w');
    fprintf(fileID, '%6.2f, %12.8f, %12.8f, %12.8f, %12.8f, %12.8f\r\n', results);
%%
    %Reasonable learning rates: between 0.010000000000000 -  0.201
    %Coarse search
    n_epochs = 3;
    e_min =   log10(0.001); e_max = log10(0.201);
    l_min = -3;
    l_max = 0;
    results = cell(100,1);
    parfor run = 1:100
        run
        e = e_min + (e_max - e_min)*rand(1, 1);
        eta = 10^e;
        l = l_min + (l_max - l_min)*rand(1, 1);
        lambda = 10^l;
        [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta, lambda, n_epochs, 1);
        
        results{run} = [eta, lambda, acc_val]
    end
     results = sortrows(cell2mat(results), 6)
     fileID = fopen('coarse search.txt','w');
     fprintf(fileID, '%12.8f, %12.8f, %12.8f, %12.8f, %12.8f, %12.8f\r\n', results');

     %%
    %Reasonable learning rates: between 0.033472281751243 - 0.043343017181802
    %Reasonable lambda: between 0.001850805948389 - 0.002095830607991
    %finer search
    n_epochs = 4;
    e_min = log10(0.065255090665790); e_max = log10( 0.094328610833744);
    l_min = log10(0.001339148845673); l_max = log10(0.001736184621166);
    results = cell(50,1);
    parfor run = 1:50
        run
        e = e_min + (e_max - e_min)*rand(1, 1);
        eta = 10^e;
        l = l_min + (l_max - l_min)*rand(1, 1);
        lambda = 10^l;
        [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta, lambda, n_epochs, 1);
        results{run} = [eta, lambda, acc_val]
    end
    results = sortrows(cell2mat(results), 7)
    fileID = fopen('finer_search.txt','w');
    fprintf(fileID, '%12.12f, %12.12f, %12.12f, %12.12f, %12.12f, %12.12f, %12.12f\r\n', results');


    %%
    % Sanity check, check that cost aproaches zero
    n_epochs = 1000;
    
    eta = 0.1; lambda = 0; testX = X(:,1:100); testY = Y(:,1:100); testy = y(:,1:100);
    [W, W_moment, b, b_moment, rho, decay] = InitParams(testX, testY);
    for epoch = 1:n_epochs
        [grad_W, grad_b] = ComputeGradients(testX, testY, W, b, lambda);
        %Momentum must be added
        [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay);  
        epoch
        cost = ComputeCost(testX, testY, W, b, lambda)
        acc = ComputeAccuracy(testX, testy, W, b)
    end
    %%
    % Sanity check, check that cost aproaches zero
    n_epochs = 1000;
    rng(400);
    eta = 0.1; lambda = 0.00001; testX = X(:,1:10); testY = Y(:,1:10); testy = y(:,1:10);
    [W, W_moment, b, b_moment, rho, decay] = InitParams(testX, testY);
    cost = ComputeCostBN(testX, testY, W, b, lambda, 1)
    for epoch = 1:n_epochs
        if epoch == 1
            [sh, u, v, s, x, P] = ForwardPassBN(testX, W, b, 1);
            [grad_W, grad_b, moving_mean, moving_var] = BackwardsPassBN(testX,testY, W, b, lambda, u, v);
        else 
            [grad_W, grad_b, moving_mean, moving_var] = BackwardsPassBN(testX,testY, W, b, lambda, moving_mean, moving_var);
        end
        
        %Momentum must be added
        [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay);  
        epoch
        cost = ComputeCostBN(testX, testY, W, b, lambda, 1, moving_mean, moving_var)
        acc = ComputeAccuracyBN(testX, testy, W, b, moving_mean, moving_var)
    end
    %%
    %Check gradients BN
    rng(400);
    lambda = 0.0;
    testX = X(1:100,1:10);
    testY = Y(:,1:10);
    [W, W_moment, b, b_moment, rho, decay] = InitParams(testX, testY);
    [sh, u, v, s, x, P] = ForwardPassBN(testX, W, b, 1);
    [grad_W, grad_b] = BackwardsPassBN(testX, testY, W, b, lambda, u, v);
    [ngrad_b, ngrad_W] = ComputeGradsNumSlow(testX, testY, W, b, lambda, 1e-6, 1);
    diff = [];
    for i = 1:3
        diff = [diff, DiffGrads(grad_W{i}, ngrad_W{i})];
    end
    diff
    %%
    %Check gradients
    rng(400);
    lambda = 0;
    testX = X(1:100,1:100);
    testY = Y(:,1:100);
    [W, W_moment, b, b_moment, rho, decay] = InitParams(testX, testY);
    [s, x, P] = EvaluateClassifier(testX, W, b);
    [grad_W, grad_b] = ComputeGradients(testX, testY, W, b, lambda);
    [ngrad_b, ngrad_W] = ComputeGradsNumSlow(testX, testY, W, b, lambda, 1e-5, 0);
    diff = [];
    for i = 1:3
        diff = [diff, DiffGrads(grad_W{i}, ngrad_W{i})];
    end
    diff
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%THE INTERESTING FUNCTIONS: BEGIN
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [Wstar, bstar, cost_train, cost_val, acc_val] = BatchNormGD( X, Y, vX, vY, vy ,n_batch, n_epochs, eta, rho, decay, W, b, lambda, W_moment, b_moment, batch_norm)
%Performs a mini-batch gradient descent 
    N =size(X,2);
    cost_train = [ComputeCostBN(X, Y, W, b, lambda, batch_norm)];
    cost_val = [ComputeCostBN(vX, vY, W, b, lambda, batch_norm)];
    acc_val = [ComputeAccuracyBN(vX, vy, W, b)];
    for epoch = 1:n_epochs
        epoch
        for j=1:N/n_batch
            j_start = (j-1)*n_batch + 1;
            j_end = j*n_batch;
            inds = j_start:j_end;
            Xbatch = X(:, j_start:j_end);
            Ybatch = Y(:, j_start:j_end);
            if j == 1
                [sh, u, v, s, x, P] = ForwardPassBN(X, W, b, batch_norm);
                [grad_W, grad_b, moving_mean, moving_var] = BackwardsPassBN(Xbatch,Ybatch, W, b, lambda, u, v);
            else 
                [grad_W, grad_b, moving_mean, moving_var] = BackwardsPassBN(Xbatch,Ybatch, W, b, lambda, moving_mean, moving_var);
            end
            [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay, epoch);
        end
       
        cost_train = [cost_train, ComputeCostBN(X, Y, W, b, lambda, batch_norm, moving_mean, moving_var)];
        cost_val = [cost_val, ComputeCostBN(vX, vY, W, b, lambda, batch_norm, moving_mean, moving_var)];
        acc_val = [acc_val, ComputeAccuracyBN(vX, vy, W, b, moving_mean, moving_var)]
    end
    Wstar = W;
    bstar = b;
end
function [grad_W, grad_b, moving_mean, moving_var] = BackwardsPassBN(X, Y, W, b, lambda, moving_mean, moving_var)
% Computes the gradients analytically with BN  
    alpha = 0.99;
    [sh, u, v, s, x, P] = ForwardPassBN(X, W, b, 1);
    for j = 1:size(W, 2)
        grad_W{j} = zeros(size(W{j}));
        grad_b{j} = zeros(size(b{j}));
    end
    for i = 1:size(X,2)
       %Last layer without bn
       g = -(Y(:,i)'/(Y(:,i)'*P(:,i))) * (diag(P(:,i))-P(:,i)*P(:,i)');
       grad_b{size(W, 2)} = grad_b{size(W, 2)} + g';
       grad_W{size(W, 2)} = grad_W{size(W, 2)} + g' * x{size(W, 2)}(:,i)'+ 2*lambda*W{size(W, 2)};
       g = g * W{size(W, 2)};
       g = g .* sign(max(zeros(size(sh{size(W, 2)-1}(:,i))),sh{size(W, 2)-1}(:,i)))';
       
       %Layers with bn
       for j = size(W, 2)-1:-1:1
           %moving mean and var
           moving_mean{j} = moving_mean{j} * alpha + (1-alpha) * u{j};
           moving_var{j} = moving_var{j} * alpha + (1-alpha) * v{j};
           g = BatchNormBackPass(g, s{j}(:,i), u{j}, v{j});
           grad_b{j} = grad_b{j} + g';
           grad_W{j} = grad_W{j} + g' * x{j}(:,i)'+ 2*lambda*W{j};
           if j>1
               %Propagate gradients
               g = g * W{j};
               g = g .* sign(max(zeros(size(sh{j-1}(:,i))),sh{j-1}(:,i)))';
           end
       end
    end
    for j = size(W, 2):-1:1
        grad_W{j} = grad_W{j} / size(X,2);
        grad_b{j} = grad_b{j} / size(X,2);
    end
end
function g = BatchNormBackPass(g, s, mean, variance)
    %somethings funky
    ub = (s - mean);
    vb = sqrt(variance+1e-6);
    n = size(g,2);
    g = g';
    du = 1./vb .* g;
    dvar = 0.5 * -1./ ((vb.^3).* sum(ub .*g));
    du = du + 2 * sum(1 / n * dvar .* ub);
    g = du+ 1 / n * ones((size(du))) * - sum(du);
    g = g';
end

function [sh, u, variance, s, x, P] = ForwardPassBN(X, W, b, BN, varargin)
%Calculates the percentages for each label for each sample and averages etc. Could be used
%for both BN and not
    x{1} = X;
    for i = 2:size(W,2)
        s{i-1} = W{i-1} * x{i-1} + diag(b{i-1})*ones(size(W{i-1},1), size(x{i-1},2));
        
        %Batch Normalize
        u{i-1} = mean(s{i-1},2);
        variance{i-1} = var(s{i-1},0,2)*(size(X,2)-1)/size(X,2);
        
        if isempty(varargin)
            Vb = diag((variance{i-1}+1e-6));
            sh{i-1} = Vb^(-1/2)*(s{i-1}-diag(u{i-1})*ones(size(s{i-1})));
        else
            Vb = diag((varargin{2}{i-1}+1e-6));
            sh{i-1} = Vb^(-1/2)*(s{i-1}-diag(varargin{1}{i-1})*ones(size(s{i-1})));
        end
        if BN == 0
            x{i} = max(zeros(size(s{i-1})), s{i-1});
        else
            x{i} = max(zeros(size(sh{i-1})), sh{i-1});
        end      
    end
    s{size(W,2)} = W{size(W,2)} * x{size(x,2)} + diag(b{size(W,2)})*ones(size(W{size(W,2)},1), size(X,2));
    P = softmax(s{size(W,2)});
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%THE INTERESTING FUNCTIONS: END
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [X, Y, y] =  LoadBatch(filename)
%Reads in data
%X = d x N, Y = K x N, K = # labels, y = N, 
    raw_data = load(filename);
    nr_samples = size(raw_data.labels,1);

    X = mat2gray(double(raw_data.data))';
    y = double(raw_data.labels+1)';
    Y = full(ind2vec(y));
end
function [W, W_moment, b, b_moment, rho, decay] = InitParams(X, Y)
    %Initializes most parameters
    rho =0.9; decay = 0.95;
    m=50; d=size(X,1); n=30; K = 10; 
    %3 layer network
    weight_params = [m,d; n,m; K,n];
    %2 layer network
    %weight_params = [n,d; K,n];
    for i = 1:size(weight_params,1)
        dims = weight_params(i, :);
        W{i} = .001*randn(dims);
        W_moment{i} = zeros(size(W{i}));
        b{i} = zeros(dims(1),1);
        b_moment{i} = zeros(size(b{i}));
    end
end
function acc = ComputeAccuracy(X, y, W, b)
%Computes the accuracy of a network using eq. 4 in the assignment
    [~,~,K_star] = EvaluateClassifier(X, W, b);
    [~, K_star] = max(K_star, [], 1);
    acc = sum(y == K_star)/size(X,2);
end
function acc = ComputeAccuracyBN(X, y, W, b, varargin)
%Computes the accuracy of a network using eq. 4 in the assignment
    if isempty(varargin)
        [~,~,~,~,~, K_star] = ForwardPassBN(X, W, b, 1);
    else
        [~,~,~,~,~, K_star] = ForwardPassBN(X, W, b, 1, varargin{1}, varargin{2});
    end
    [~, K_star] = max(K_star, [], 1);
    acc = sum(y == K_star)/size(X,2);
end
function J = ComputeCostBN(X, Y, W, b, lambda, BN, varargin)
%Computes cost using for BN and without
    if isempty(varargin)
        [~,~,~,~,~, P] = ForwardPassBN(X, W, b, BN);
    else
        [~,~,~,~,~, P] = ForwardPassBN(X, W, b, BN, varargin{1}, varargin{2});
    end
    l_cross = sum(-log(diag(Y'*P)));
    J = sum(l_cross)/size(X,2);
    for i = size(W,2)
        J = J + lambda*(sum(sum(W{i}.*W{i})));
    end

end
function [grad_W, grad_b] = ComputeGradients(X, Y, W, b, lambda)
%Computes the gradients analytically without BN  
    for j = 1:size(W, 2)
        grad_W{j} = zeros(size(W{j}));
        grad_b{j} = zeros(size(b{j}));
    end
    [sh, u, v, s, x, P] = ForwardPassBN(X, W, b, 0);
    for i = 1:size(X,2)
       %Add gradients
       g = -(Y(:,i)'/(Y(:,i)'*P(:,i))) * (diag(P(:,i))-P(:,i)*P(:,i)');
       for j = size(W, 2):-1:1
           grad_b{j} = grad_b{j} + g';
           grad_W{j} = grad_W{j} + g' * x{j}(:,i)'+ 2*lambda*W{j};
           if j>1
               %Propagate gradients
               g = g * W{j};
               g = g * diag(sign(x{j}(:,i)));
           end
       end
    end
    for j = size(W, 2):-1:1
        grad_W{j} = grad_W{j} / size(X,2);
        grad_b{j} = grad_b{j} / size(X,2);
    end
end
function [grad_b, grad_W] = ComputeGradsNumSlow(X, Y, W, b, lambda, h, BN)
    grad_W = cell(numel(W), 1);
    grad_b = cell(numel(b), 1);

    for j=1:length(b)
        grad_b{j} = zeros(size(b{j}));

        for i=1:length(b{j})

            b_try = b;
            b_try{j}(i) = b_try{j}(i) - h;
            c1 = ComputeCostBN(X, Y, W, b_try, lambda, BN);

            b_try = b;
            b_try{j}(i) = b_try{j}(i) + h;
            c2 = ComputeCostBN(X, Y, W, b_try, lambda, BN);

            grad_b{j}(i) = (c2-c1) / (2*h);
        end
    end

    for j=1:length(W)
        grad_W{j} = zeros(size(W{j}));

        for i=1:numel(W{j})

            W_try = W;
            W_try{j}(i) = W_try{j}(i) - h;
            c1 = ComputeCostBN(X, Y, W_try, b, lambda, BN);
            W_try = W;
            W_try{j}(i) = W_try{j}(i) + h;
            c2 = ComputeCostBN(X, Y, W_try, b, lambda, BN);

            grad_W{j}(i) = (c2-c1) / (2*h);
        end
    end
end
function error = DiffGrads( grad, grad_num )
%Calculates the difference between 2 gradient matrices. Used to confirm
%analytical gradients
    error = sum(sum(abs(grad - grad_num)))/numel(grad_num);
end
function [s, x, P] = EvaluateClassifier(X, W, b)
%Calculates the percentages for each label for each sample for none BN
%networks
    x{1} = X;
    for i = 2:size(W,2)
        s{i-1} = W{i-1} * x{i-1} + diag(b{i-1})*ones(size(W{i-1},1), size(x{i-1},2));
        x{i} = max(zeros(size(s{i-1})), s{i-1});
    end
    s{size(W,2)} = W{size(W,2)} * x{size(x,2)} + diag(b{size(W,2)})*ones(size(W{size(W,2)},1), size(X,2));
    P = softmax(s{size(W,2)});
end
function [Wstar, bstar, cost_train, cost_val, acc_val] = MiniBatchGD( X, Y, vX, vY, vy ,n_batch, n_epochs, eta, rho, decay, W, b, lambda, W_moment, b_moment, batch_norm)
%Performs a mini-batch gradient descent witout BN
    N =size(X,2);
    cost_train = [ComputeCostBN(X, Y, W, b, lambda, batch_norm)];
    cost_val = [ComputeCostBN(vX, vY, W, b, lambda, batch_norm)];
    acc_val = [ComputeAccuracy(vX, vy, W, b)];
    for epoch = 1:n_epochs
        epoch
        for j=1:N/n_batch
            j_start = (j-1)*n_batch + 1;
            j_end = j*n_batch;
            inds = j_start:j_end;
            Xbatch = X(:, j_start:j_end);
            Ybatch = Y(:, j_start:j_end);
            [grad_W, grad_b] = ComputeGradients(Xbatch,Ybatch, W, b, lambda);
            [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay, epoch);
        end
        
        cost_train = [cost_train, ComputeCostBN(X, Y, W, b, lambda, batch_norm)];
        cost_val = [cost_val, ComputeCostBN(vX, vY, W, b, lambda, batch_norm)];
        acc_val = [acc_val, ComputeAccuracy(vX, vy, W, b)]
        
    end
    Wstar = W;
    bstar = b;
end
function [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, valX, valY, valy, eta, lambda, n_epochs, batch_norm)
%TESTNET Easy training of nets with different hyperparams
    n_batch = 100;
    [W, W_moment,b, b_moment, rho, decay] = InitParams(X, Y);
    if batch_norm == 0
        [Wstar, bstar, cost_train, cost_val, acc_val] = MiniBatchGD( X, Y, valX, valY, valy,n_batch, n_epochs, eta, rho, decay, W, b, lambda, W_moment, b_moment, batch_norm);
    else 
        [Wstar, bstar, cost_train, cost_val, acc_val] = BatchNormGD( X, Y, valX, valY, valy,n_batch, n_epochs, eta, rho, decay, W, b, lambda, W_moment, b_moment, batch_norm);
    end
end
function [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay, epoch)
%UPDATE Updates the parameters
    for i = 1:size(W,2)
        W_moment{i} = rho*W_moment{i} + eta * grad_W{i};
        b_moment{i} = rho*b_moment{i} + eta * grad_b{i};
        W{i} = W{i} - W_moment{i};
        b{i} = b{i} - b_moment{i};
    end
end






