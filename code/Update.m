function [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay, epoch)
%UPDATE Updates the parameters
    for i = 1:size(W,2)
        W_moment{i} = rho*W_moment{i} + eta * grad_W{i};
        b_moment{i} = rho*b_moment{i} + eta * grad_b{i};
        W{i} = W{i} - W_moment{i};
        b{i} = b{i} - b_moment{i};
    end
end

