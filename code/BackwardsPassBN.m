function [grad_W, grad_b, moving_mean, moving_var] = BackwardsPassBN(X, Y, W, b, lambda, moving_mean, moving_var)
% Computes the gradients analytically with BN  
    alpha = 0.99;
    [sh, u, v, s, x, P] = ForwardPassBN(X, W, b, 1);
    for j = 1:size(W, 2)
        grad_W{j} = zeros(size(W{j}));
        grad_b{j} = zeros(size(b{j}));
    end
    for i = 1:size(X,2)
       %Last layer without bn
       g = -(Y(:,i)'/(Y(:,i)'*P(:,i))) * (diag(P(:,i))-P(:,i)*P(:,i)');
       grad_b{size(W, 2)} = grad_b{size(W, 2)} + g';
       grad_W{size(W, 2)} = grad_W{size(W, 2)} + g' * x{size(W, 2)}(:,i)'+ 2*lambda*W{size(W, 2)};
       g = g * W{size(W, 2)};
       g = g .* sign(max(zeros(size(sh{size(W, 2)-1}(:,i))),sh{size(W, 2)-1}(:,i)))';
       
       %Layers with bn
       for j = size(W, 2)-1:-1:1
           %moving mean and var
           moving_mean{j} = moving_mean{j} * alpha + (1-alpha) * u{j};
           moving_var{j} = moving_var{j} * alpha + (1-alpha) * v{j};
           g = BatchNormBackPass(g, s{j}(:,i), u{j}, v{j});
           grad_b{j} = grad_b{j} + g';
           grad_W{j} = grad_W{j} + g' * x{j}(:,i)'+ 2*lambda*W{j};
           if j>1
               %Propagate gradients
               g = g * W{j};
               g = g .* sign(max(zeros(size(sh{j-1}(:,i))),sh{j-1}(:,i)))';
           end
       end
    end
    for j = size(W, 2):-1:1
        grad_W{j} = grad_W{j} / size(X,2);
        grad_b{j} = grad_b{j} / size(X,2);
    end
end
function g = BatchNormBackPass(g, s, mean, variance)
    %somethings funky
    ub = (s - mean);
    vb = sqrt(variance+1e-6);
    n = size(g,2);
    g = g';
    du = 1./vb .* g;
    dvar = 0.5 * -1./ ((vb.^3).* sum(ub .*g));
    du = du + 2 * sum(1 / n * dvar .* ub);
    g = du+ 1 / n * ones((size(du))) * - sum(du);
    g = g';
end
