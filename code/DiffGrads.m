function error = DiffGrads( grad, grad_num )
%Calculates the difference between 2 gradient matrices. Used to confirm
%analytical gradients
    error = sum(sum(abs(grad - grad_num)))/numel(grad_num);
end

