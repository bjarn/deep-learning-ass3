function Main()
    %THE MAIN SCRIPT
    %LOADING DATA
    [X, Y, y] = LoadBatch('data_batch_1.mat');
    [vX, vY, vy] = LoadBatch('data_batch_2.mat');
    [tX, tY, ty] = LoadBatch('test_batch.mat');
    %Normalize data
    X_mean = mean(X,2);
    X = double(X) - repmat(X_mean, [1, size(X, 2)]);
    vX = vX - repmat(X_mean, [1, size(vX, 2)]);
    tX = tX - repmat(X_mean, [1, size(tX, 2)]);
    format long
    %%
    %Test the finer hyperparam values
    n_epochs = 5;
    %eta = 0.036826249945; lambda = 0.001883553952;
    eta = 0.065916629041086; lambda =  0.001369941385255;
    [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta, lambda, n_epochs, 0);
    [Wstarbn, bstarbn, cost_trainbn, cost_valbn, acc_valbn] = TestNet( X, Y, y, vX, vY, vy, eta, lambda, n_epochs, 1);
    
    %%
    %Test the 2 layer with and without BN and differing eta(low, med, high)
    n_epochs = 10;
    eta = [0.01, 0.1, 0.3];
    lambda =  0.001;
    parfor i = 1:size(eta,2)
        [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta(i), lambda, n_epochs, 1);
        resultstrain{i} = [eta(i), 0, cost_train]
        resultsval{i} = [eta(i), 0, cost_val]
    end
    %%
    %Plot results
    figure;
    etan = 3;
    plot(0:n_epochs,resultsval{etan}(3:end), 'g',0:n_epochs,resultstrain{etan}(3:end),'b')
    str=sprintf('Cost on val-set(g) and train-set(b) with BN and eta = %f', resultsval{etan}(1));
    title(str);
    xlabel('Epochs');
    ylabel('Cost');
    
    %%
    %All training available
    n_epochs = 20;
    trainX = [X, vX, tX(:,1:9000)];
    trainY = [Y, vY, tY(:,1:9000)];
    trainy = [y, vy, ty(:,1:9000)];
    testX = tX(:,9001:end);
    testY = tY(:,9001:end);
    testy = ty(:,9001:end);
    %eta = 0.036826249945; lambda = 0.001883553952;
    eta = 0.065916629041086; lambda = 0.001369941385255;
    [Wstarbn, bstarbn, cost_trainbn, cost_valbn, acc_valbn] = TestNet( trainX, trainY, trainy, testX, testY, testy, eta, lambda, n_epochs, 1);
    %[Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( trainX, trainY, trainy, testX, testY, testy, eta, lambda, n_epochs, 0);
        %%
    % Plot cost on train and validation set, and accuracy
    figure;
    plot(0:n_epochs,cost_valbn, 'g',0:n_epochs,cost_val,'b')
    %axis([1 n_epochs 1.6 2.3])
    str=sprintf('Comparing cost with/without BN: Lambda = %f and eta = %f', lambda, eta);
    title(str);
    xlabel('Epochs');
    ylabel('Cost');
    
    figure;
    plot(0:n_epochs,acc_valbn, 'g',0:n_epochs,acc_val,'b')
    str=sprintf('Comparing accuracy with/without BN: Lambda = %f and eta = %f', lambda, eta);
    title(str);
    xlabel('Epochs');
    ylabel('Accuracy');
      %%
    % Plot cost on train and validation set, and accuracy
    figure;
    plot(0:n_epochs,cost_valbn, 'g')
    str=sprintf('Cost with BN: Lambda = %f and eta = %f', lambda, eta);
    title(str);
    xlabel('Epochs');
    ylabel('Cost');
    figure;
    plot(0:n_epochs,acc_valbn, 'g')
    str=sprintf('Accuracy with BN: Lambda = %f and eta = %f', lambda, eta);
    title(str);
    xlabel('Epochs');
    ylabel('Accuracy');
    %%
    %Find reasonable learning rates
    n_epochs = 4;
    
    eta = 0.001:.1:1;
    results = cell(size(eta,2), 1);
    parfor i = 1:size(eta,2)
        [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta(i), 0.000001, n_epochs, 1);
        results{i} = [eta(i), cost_val]
    end
    results = cell2mat(results)
    fileID = fopen('reasonable_learning.txt','w');
    fprintf(fileID, '%6.2f, %12.8f, %12.8f, %12.8f, %12.8f, %12.8f\r\n', results);
%%
    %Reasonable learning rates: between 0.010000000000000 -  0.201
    %Coarse search
    n_epochs = 3;
    e_min =   log10(0.001); e_max = log10(0.201);
    l_min = -3;
    l_max = 0;
    results = cell(100,1);
    parfor run = 1:100
        run
        e = e_min + (e_max - e_min)*rand(1, 1);
        eta = 10^e;
        l = l_min + (l_max - l_min)*rand(1, 1);
        lambda = 10^l;
        [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta, lambda, n_epochs, 1);
        
        results{run} = [eta, lambda, acc_val]
    end
     results = sortrows(cell2mat(results), 6)
     fileID = fopen('coarse search.txt','w');
     fprintf(fileID, '%12.8f, %12.8f, %12.8f, %12.8f, %12.8f, %12.8f\r\n', results');

     %%
    %Reasonable learning rates: between 0.033472281751243 - 0.043343017181802
    %Reasonable lambda: between 0.001850805948389 - 0.002095830607991
    %finer search
    n_epochs = 4;
    e_min = log10(0.065255090665790); e_max = log10( 0.094328610833744);
    l_min = log10(0.001339148845673); l_max = log10(0.001736184621166);
    results = cell(50,1);
    parfor run = 1:50
        run
        e = e_min + (e_max - e_min)*rand(1, 1);
        eta = 10^e;
        l = l_min + (l_max - l_min)*rand(1, 1);
        lambda = 10^l;
        [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, vX, vY, vy, eta, lambda, n_epochs, 1);
        results{run} = [eta, lambda, acc_val]
    end
    results = sortrows(cell2mat(results), 7)
    fileID = fopen('finer_search.txt','w');
    fprintf(fileID, '%12.12f, %12.12f, %12.12f, %12.12f, %12.12f, %12.12f, %12.12f\r\n', results');


    %%
    % Sanity check, check that cost aproaches zero
    n_epochs = 10;
    
    eta = 0.1; lambda = 0; testX = X(:,1:100); testY = Y(:,1:100); testy = y(:,1:100);
    [W, W_moment, b, b_moment, rho, decay] = InitParams(testX, testY);
    for epoch = 1:n_epochs
        [grad_W, grad_b] = ComputeGradients(testX, testY, W, b, lambda);
        %Momentum must be added
        [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay);  
        epoch
        cost = ComputeCost(testX, testY, W, b, lambda)
        acc = ComputeAccuracy(testX, testy, W, b)
    end
    %%
    % Sanity check, check that cost aproaches zero
    n_epochs = 1000;
    rng(400);
    eta = 0.1; lambda = 0.00001; testX = X(:,1:10); testY = Y(:,1:10); testy = y(:,1:10);
    [W, W_moment, b, b_moment, rho, decay] = InitParams(testX, testY);
    cost = ComputeCostBN(testX, testY, W, b, lambda, 1)
    for epoch = 1:n_epochs
        if epoch == 1
            [sh, u, v, s, x, P] = ForwardPassBN(testX, W, b, 1);
            [grad_W, grad_b, moving_mean, moving_var] = BackwardsPassBN(testX,testY, W, b, lambda, u, v);
        else 
            [grad_W, grad_b, moving_mean, moving_var] = BackwardsPassBN(testX,testY, W, b, lambda, moving_mean, moving_var);
        end
        
        %Momentum must be added
        [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay);  
        epoch
        cost = ComputeCostBN(testX, testY, W, b, lambda, 1, moving_mean, moving_var)
        acc = ComputeAccuracyBN(testX, testy, W, b, moving_mean, moving_var)
    end
    %%
    %Check gradients
    rng(400);
    lambda = 0.0;
    testX = X(1:100,1:10);
    testY = Y(:,1:10);
    [W, W_moment, b, b_moment, rho, decay] = InitParams(testX, testY);
    [sh, u, v, s, x, P] = ForwardPassBN(testX, W, b, 1);
    [grad_W, grad_b] = BackwardsPassBN(testX, testY, W, b, lambda, u, v);
    [ngrad_b, ngrad_W] = ComputeGradsNumSlow(testX, testY, W, b, lambda, 1e-6, 1);
    diff = [];
    for i = 1:3
        diff = [diff, DiffGrads(grad_W{i}, ngrad_W{i})];
    end
    diff
    %%
    %Check gradients
    rng(400);
    lambda = 0;
    testX = X(1:100,1:100);
    testY = Y(:,1:100);
    [W, W_moment, b, b_moment, rho, decay] = InitParams(testX, testY);
    [s, x, P] = EvaluateClassifier(testX, W, b);
    [grad_W, grad_b] = ComputeGradients(testX, testY, W, b, lambda);
    [ngrad_b, ngrad_W] = ComputeGradsNumSlow(testX, testY, W, b, lambda, 1e-5, 0);
    diff = [];
    for i = 1:3
        diff = [diff, DiffGrads(grad_W{i}, ngrad_W{i})];
    end
    diff
end
