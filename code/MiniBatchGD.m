function [Wstar, bstar, cost_train, cost_val, acc_val] = MiniBatchGD( X, Y, vX, vY, vy ,n_batch, n_epochs, eta, rho, decay, W, b, lambda, W_moment, b_moment, batch_norm)
%Performs a mini-batch gradient descent 
    N =size(X,2);
    cost_train = [ComputeCostBN(X, Y, W, b, lambda, batch_norm)];
    cost_val = [ComputeCostBN(vX, vY, W, b, lambda, batch_norm)];
    acc_val = [ComputeAccuracy(vX, vy, W, b)];
    for epoch = 1:n_epochs
        epoch
        for j=1:N/n_batch
            j_start = (j-1)*n_batch + 1;
            j_end = j*n_batch;
            inds = j_start:j_end;
            Xbatch = X(:, j_start:j_end);
            Ybatch = Y(:, j_start:j_end);
            [grad_W, grad_b] = ComputeGradients(Xbatch,Ybatch, W, b, lambda);
            [W, b, W_moment, b_moment, eta] = Update( W, b, grad_W, grad_b, W_moment, b_moment, lambda, eta, rho, decay, epoch);
        end
        
        cost_train = [cost_train, ComputeCostBN(X, Y, W, b, lambda, batch_norm)];
        cost_val = [cost_val, ComputeCostBN(vX, vY, W, b, lambda, batch_norm)];
        acc_val = [acc_val, ComputeAccuracy(vX, vy, W, b)]
        
    end
    Wstar = W;
    bstar = b;
end

