function [grad_W, grad_b] = ComputeGradients(X, Y, W, b, lambda)
%Computes the gradients analytically using the procedure shown i lec 3.  
    for j = 1:size(W, 2)
        grad_W{j} = zeros(size(W{j}));
        grad_b{j} = zeros(size(b{j}));
    end
    [sh, u, v, s, x, P] = ForwardPassBN(X, W, b, 0);
    for i = 1:size(X,2)
       %Add gradients
       g = -(Y(:,i)'/(Y(:,i)'*P(:,i))) * (diag(P(:,i))-P(:,i)*P(:,i)');
       for j = size(W, 2):-1:1
           grad_b{j} = grad_b{j} + g';
           grad_W{j} = grad_W{j} + g' * x{j}(:,i)'+ 2*lambda*W{j};
           if j>1
               %Propagate gradients
               g = g * W{j};
               g = g * diag(sign(x{j}(:,i)));
           end
       end
    end
    for j = size(W, 2):-1:1
        grad_W{j} = grad_W{j} / size(X,2);
        grad_b{j} = grad_b{j} / size(X,2);
    end
end

