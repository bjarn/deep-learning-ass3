function [X, Y, y] =  LoadBatch(filename)
%Reads in data
%X = d x N, Y = K x N, K = # labels, y = N, 
    raw_data = load(filename);
    nr_samples = size(raw_data.labels,1);

    X = mat2gray(double(raw_data.data))';
    y = double(raw_data.labels+1)';
    Y = full(ind2vec(y));
end

