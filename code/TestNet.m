function [Wstar, bstar, cost_train, cost_val, acc_val] = TestNet( X, Y, y, valX, valY, valy, eta, lambda, n_epochs, batch_norm)
%TESTNET Easy training of nets with different hyperparams
    n_batch = 100;
    [W, W_moment,b, b_moment, rho, decay] = InitParams(X, Y);
    if batch_norm == 0
        [Wstar, bstar, cost_train, cost_val, acc_val] = MiniBatchGD( X, Y, valX, valY, valy,n_batch, n_epochs, eta, rho, decay, W, b, lambda, W_moment, b_moment, batch_norm);
    else 
        [Wstar, bstar, cost_train, cost_val, acc_val] = BatchNormGD( X, Y, valX, valY, valy,n_batch, n_epochs, eta, rho, decay, W, b, lambda, W_moment, b_moment, batch_norm);
    end
end

