function [sh, u, variance, s, x, P] = ForwardPassBN(X, W, b, BN, varargin)
%Calculates the percentages for each label for each sample. COuld be used
%for both BN and not
    x{1} = X;
    for i = 2:size(W,2)
        s{i-1} = W{i-1} * x{i-1} + diag(b{i-1})*ones(size(W{i-1},1), size(x{i-1},2));
        
        %Batch Normalize
        u{i-1} = mean(s{i-1},2);
        variance{i-1} = var(s{i-1},0,2)*(size(X,2)-1)/size(X,2);
        
        if isempty(varargin)
            Vb = diag((variance{i-1}+1e-6));
            sh{i-1} = Vb^(-1/2)*(s{i-1}-diag(u{i-1})*ones(size(s{i-1})));
        else
            Vb = diag((varargin{2}{i-1}+1e-6));
            sh{i-1} = Vb^(-1/2)*(s{i-1}-diag(varargin{1}{i-1})*ones(size(s{i-1})));
        end
        if BN == 0
            x{i} = max(zeros(size(s{i-1})), s{i-1});
        else
            x{i} = max(zeros(size(sh{i-1})), sh{i-1});
        end      
    end
    s{size(W,2)} = W{size(W,2)} * x{size(x,2)} + diag(b{size(W,2)})*ones(size(W{size(W,2)},1), size(X,2));
    P = softmax(s{size(W,2)});
end

