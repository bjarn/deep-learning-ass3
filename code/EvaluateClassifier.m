function [s, x, P] = EvaluateClassifier(X, W, b)
%Calculates the percentages for each label for each sample for none BN
%networks
    x{1} = X;
    for i = 2:size(W,2)
        s{i-1} = W{i-1} * x{i-1} + diag(b{i-1})*ones(size(W{i-1},1), size(x{i-1},2));
        x{i} = max(zeros(size(s{i-1})), s{i-1});
    end
    s{size(W,2)} = W{size(W,2)} * x{size(x,2)} + diag(b{size(W,2)})*ones(size(W{size(W,2)},1), size(X,2));
    P = softmax(s{size(W,2)});
end

